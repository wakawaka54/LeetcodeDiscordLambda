using LeetcodeDiscordLambda.Resources;
using Xunit;

namespace LeetcodeDiscordLambda.Tests.Resources
{
    public class ResourceReaderTest
    {
        private readonly ResourceReader _resourceReader = new ResourceReader();

        [Fact]
        public void CanOpenResourceFile()
        {
            var content = _resourceReader.ReadString("DiscordProblemTemplate.liquid");
            Assert.NotEmpty(content);
        }
    }
}