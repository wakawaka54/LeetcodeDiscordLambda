using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using LeetcodeDiscordLambda;
using LeetcodeDiscordLambda.Core.Json;
using LeetcodeDiscordLambda.Domain.Questions;
using LeetcodeDiscordLambda.Domain.Questions.Destination;
using LeetcodeDiscordLambda.Services.Leetcode.Types;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit.Abstractions;

namespace LeetcodeDiscordLambda.Tests
{
    public class LeetcodeDiscordLambdaHandlerTest
    {
        private readonly ITestOutputHelper _output;
        private readonly Mock<IApplicationComponent> _applicationComponent = new Mock<IApplicationComponent>();
        private readonly Mock<IQuestionEvaluator> _questionEvaluator = new Mock<IQuestionEvaluator>();
        private readonly Mock<IQuestionPublisher> _questionPublisher = new Mock<IQuestionPublisher>();
        private readonly JsonMapper _jsonMapper = new JsonMapper();

        public LeetcodeDiscordLambdaHandlerTest(ITestOutputHelper testOutputHelper)
        {
            _output = testOutputHelper;

            _applicationComponent.Setup(component => component.QuestionEvaluator).Returns(_questionEvaluator.Object);
            _applicationComponent.Setup(component => component.QuestionPublisher).Returns(_questionPublisher.Object);
            _applicationComponent.Setup(component => component.JsonMapper).Returns(_jsonMapper);
        }
        
        [Fact]
        public async Task LambdaHandlerSelectsQuestionAndPostsIt()
        {
            var function = new LeetcodeDiscordLambdaHandler(_applicationComponent.Object);
            var context = new TestLambdaContext();
            var question = CreateQuestion();
            
            // Setup
            _questionEvaluator
                .Setup(evaluator => evaluator.Evaluate(It.IsAny<QuestionEvaluationContext>()))
                .Returns(Task.FromResult(question));
            
            _questionPublisher
                .Setup(publisher => publisher.Publish(It.IsAny<DiscordChannel>(), question))
                .Verifiable();
            
            var input = JsonSerializer.Serialize(new DiscordLeetcodePostQuestionRequest
            {
                DiscordChannelId = "test",
                DiscordWebhookUrl = "webhookUrl",
                LeetcodeDifficulties = new List<DifficultyLevel> {DifficultyLevel.Easy, DifficultyLevel.Medium},
                LeetcodeTags = new List<string> { "minimax" }
            }, JsonMapper.StandardOptions);
            
            // Test the Lambda function
            var output = await function.FunctionHandler(input.ToStream(), context);
            
            // Validate
            Assert.Equal(
                question.Id, 
                JsonSerializer.Deserialize<Question>(output.ReadToString(), JsonMapper.StandardOptions).Id
            );
            
            _questionPublisher.Verify(publisher => publisher.Publish(It.IsAny<DiscordChannel>(), question), Times.Once());
        }

        private Question CreateQuestion() =>
            new Question
            {
                Id = "MockQuestion"
            };
    }
}
