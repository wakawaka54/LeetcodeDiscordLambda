using System;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using LeetcodeDiscordLambda.Data;
using Xunit;

namespace LeetcodeDiscordLambda.Tests.Data
{
    public class CodeQuestionRepositoryTest
    {
        private const string ProblemId = "problemId";
        private static DateTime CreatedTime = new DateTime(2000, 1, 1);
        
        private readonly CodeQuestionRepository _repository;
        
        public CodeQuestionRepositoryTest()
        {
            var dynamoDb = new AmazonDynamoDBClient();
            _repository = new CodeQuestionRepository(new DynamoDBContext(dynamoDb, new DynamoDBContextConfig
            {
               TableNamePrefix = "beta-"
            }));
        }
        
        [Fact]
        public async void AbleToDeleteSaveAndGetQuestion()
        {
            await _repository.DeleteAsync(ProblemId);

            await _repository.SaveAsync(new CodeQuestion
            {
                Id = ProblemId,
                Created = CreatedTime
            });

            var codeQuestion = await _repository.GetAsync(ProblemId);
            
            Assert.Equal(CreatedTime, codeQuestion.Created);
        }
    }
}