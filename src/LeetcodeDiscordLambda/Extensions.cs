using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading.Tasks;

namespace LeetcodeDiscordLambda
{
    public static class StringExtensions
    {
        public static Stream ToStream(this string str)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(str));
        }

        public static string RemoveRepeating(this string str, char character)
        {
            var builder = new StringBuilder();
            char? last = null;
            
            foreach (var next in str)
            {
                if (last == character && next == character)
                {
                    continue;
                }

                last = next;
                builder.Append(next);
            }

            return builder.ToString();
        }
    }

    public static class StreamExtensions
    {
        public static string ReadToString(this Stream stream)
        {
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }

    public static class EnumerableExtensions
    {
        private static Random _random = new Random();
        public static string JoinToString<T>(this IEnumerable<T> list, string seperator = ",")
        {
            var strings = list.Select(s => s.ToString());
            return String.Join(seperator, strings);
        }

        public static T SelectRandom<T>(this IEnumerable<T> list)
        {
            var resolvedList = list.ToList();
            var random = _random.Next(resolvedList.Count);
            return resolvedList[random];
        }

        /// <summary>
        /// Waits for all the async tasks to be completed.
        /// </summary>
        /// <param name="asyncTasks"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> WhenAll<T>(this IEnumerable<Task<T>> asyncTasks)
        {
            return await Task.WhenAll(asyncTasks);
        }
    }

    public static class DictionaryExtensions
    {
        public static T GetOrFetch<T, K>(this IDictionary<K, T> dictionary, K key, Func<T> retriever)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary[key] = retriever();
            }

            return dictionary[key];
        }
    }
}