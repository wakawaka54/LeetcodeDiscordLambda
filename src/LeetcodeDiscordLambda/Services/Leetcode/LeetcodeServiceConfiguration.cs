using System;
using System.Net.Http;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using LeetcodeDiscordLambda.Core.DependencyInjection;
using LeetcodeDiscordLambda.Core.Json;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Services.Leetcode
{
    /// <summary>
    /// Registers the Leetcode Service into the Dependency Injection context.
    /// </summary>
    public static class LeetcodeServiceConfiguration
    {
        private static string GraphQLHttpClient = "LeetcodeGraphQLHttpClient";
        private static string HttpClient = "LeetcodeHttpClient";
        
        public static void ConfigureLeetcodeService(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddNamedService(GraphQLHttpClient, s => new GraphQLHttpClient(
                LeetcodeUris.GraphQL,
                new NewtonsoftJsonSerializer()
            ));

            serviceCollection.AddHttpClient(HttpClient, client =>
            {
                client.BaseAddress = new Uri(LeetcodeUris.Leetcode);
            });

            serviceCollection.AddSingleton<ILeetcodeService, LeetcodeService>(s => new LeetcodeService(
                s.GetNamed<GraphQLHttpClient>(GraphQLHttpClient),
                s.GetService<IHttpClientFactory>().CreateClient(HttpClient),
                s.GetRequiredService<IJsonMapper>()
            ));
        }
    }
}