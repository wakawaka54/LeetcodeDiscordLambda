using System;
using LeetcodeDiscordLambda.Services.Types;

namespace LeetcodeDiscordLambda.Services.Leetcode.Types
{
    /// <summary>
    /// An exception from Leetcode.
    /// </summary>
    public class LeetcodeServiceException : ServiceException
    {
        public LeetcodeServiceException(string message) : base(message)
        {
            
        }

        public LeetcodeServiceException(string message, Exception innerException) : base(message, innerException)
        {
            
        }
    }
}