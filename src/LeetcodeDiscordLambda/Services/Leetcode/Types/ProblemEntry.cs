using System;
using System.Text.Json.Serialization;

namespace LeetcodeDiscordLambda.Services.Leetcode.Types
{
    public class ProblemEntry
    {
        [JsonPropertyName("stat")]
        public Information Information { get; set; }
        
        [JsonPropertyName("difficulty")]
        public Difficulty Difficulty { get; set; }
        
        [JsonPropertyName("paid_only")]
        public bool IsPaidOnly { get; set; }
        
        [JsonPropertyName("is_favor")]
        public bool IsFavor { get; set; }
    }

    public class Information
    {
        [JsonPropertyName("question_id")]
        public string Id { get; set; }
        
        [JsonPropertyName("question__title")]
        public string Title { get; set; }
        
        [JsonPropertyName("question__title_slug")]
        public string Slug { get; set; }
        
        [JsonPropertyName("total_acs")]
        public int TotalSolves { get; set; }
        
        [JsonPropertyName("total_submitted")]
        public int TotalSubmitted { get; set; }
    }

    public class Difficulty
    {
        [JsonPropertyName("level")]
        public DifficultyLevel Level { get; set; }
    }
    
    public enum DifficultyLevel
    {
        Easy, Medium, Hard
    }
}