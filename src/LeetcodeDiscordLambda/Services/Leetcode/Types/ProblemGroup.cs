using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace LeetcodeDiscordLambda.Services.Leetcode.Types
{
    /// <summary>
    /// Group of Leetcode problems.
    /// </summary>
    public class ProblemGroup
    {
        [JsonPropertyName("num_total")]
        public int TotalNum { get; set; }

        [JsonPropertyName("stat_status_pairs")]
        public List<ProblemEntry> Problems { get; set; }
    }
}