using System.Collections.Generic;

namespace LeetcodeDiscordLambda.Services.Leetcode.Types
{
    /// <summary>
    /// Defines a Leetcode Problem.
    /// </summary>
    public class Problem
    {
        public string Id { get; set; }
        
        public string Link { get; set; }
        
        public string Slug { get; set; }
        public string Title { get; set; }
        
        public int Likes { get; set; }
        
        public int Dislikes { get; set; }
        public string Content { get; set; }
        
        public bool PaidOnly { get; set; }
        
        public string TestCase { get; set; }
        
        public DifficultyLevel Difficulty { get; set; }
        
        public List<string> Tags { get; set; } = new List<string>();
        public List<CodeSnippet> CodeSnippets { get; set; } = new List<CodeSnippet>();
    }

    public class CodeSnippet
    {
        public string Language { get; set; }
        public string Code { get; set; }
    }
}