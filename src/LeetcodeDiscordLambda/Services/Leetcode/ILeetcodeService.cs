using System.Threading.Tasks;
using LeetcodeDiscordLambda.Services.Leetcode.Types;

namespace LeetcodeDiscordLambda.Services.Leetcode
{
    /// <summary>
    /// Service interface for Leetcode.
    /// </summary>
    public interface ILeetcodeService
    {
        public Task<ProblemGroup> GetProblemsAsync();

        public Task<ProblemGroup> GetProblemsByTagAsync(string tag);

        public Task<Problem> GetProblem(string id);
    }
}