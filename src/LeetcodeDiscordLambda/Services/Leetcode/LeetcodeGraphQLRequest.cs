using GraphQL;

namespace LeetcodeDiscordLambda.Services.Leetcode
{
    /// <summary>
    /// GraphQL Request for GetProblemsByTag.
    /// </summary>
    internal class GetProblemsByTagGraphQLRequest : GraphQLRequest
    {
        public GetProblemsByTagGraphQLRequest(string tag) :
            base(
                @"
                        query getTopicTag($slug: String!) {
                            topicTag(slug: $slug) {
                                questions {
                                    status
                                    questionId
                                    title
                                    titleSlug
                                    stats
                                    difficulty
                                    isPaidOnly
                                    topicTags {
                                        slug
                                    }
                                }
                            }
                        }
                    ",
                new
                {
                    slug = tag
                }
            )
        {
            // do nothing.
        }
    }

    internal class GetProblemDetailGraphQLRequest : GraphQLRequest
    {
        public GetProblemDetailGraphQLRequest(string titleSlug) :
            base(
                @"
                   query getQuestionDetail($titleSlug: String!) {
                        question(titleSlug: $titleSlug) {
                            questionId
                            title
                            difficulty
                            likes
                            dislikes
                            isLiked
                            isPaidOnly
                            stats
                            status
                            content
                            topicTags {
                                name
                            }
                            codeSnippets {
                                lang
                                langSlug
                                code
                            }
                            sampleTestCase
                        }
                    }
                ",
                new
                {
                    titleSlug
                }
            )
        {
            // do nothing
        }
    }
}