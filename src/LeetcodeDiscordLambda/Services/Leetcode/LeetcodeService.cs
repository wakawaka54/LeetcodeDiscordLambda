using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Client.Abstractions.Utilities;
using GraphQL.Client.Http;
using LeetcodeDiscordLambda.Core.Json;
using LeetcodeDiscordLambda.Services.Leetcode.Types;
using Newtonsoft.Json.Linq;

namespace LeetcodeDiscordLambda.Services.Leetcode
{
    public class LeetcodeService : ILeetcodeService
    {
        private readonly HttpClient _httpClient;
        private readonly GraphQLHttpClient _graphQlHttpClient;
        private readonly IJsonMapper _jsonMapper;

        public LeetcodeService(GraphQLHttpClient graphQlHttpClient, HttpClient httpClient, IJsonMapper jsonMapper)
        {
            _graphQlHttpClient = graphQlHttpClient;
            _httpClient = httpClient;
            _jsonMapper = jsonMapper;
        }
        
        public Task<ProblemGroup> GetProblemsAsync()
        {
            return HttpRequest<ProblemGroup>(() => _httpClient.GetAsync(LeetcodeUris.AllProblems));
        }

        public async Task<ProblemGroup> GetProblemsByTagAsync(string tag)
        {
            var queryResponse = await GraphQLRequest<dynamic>(new GetProblemsByTagGraphQLRequest(tag));
            
            List<dynamic> questions = AsDynamicList(queryResponse.topicTag.questions)
                            ?? throw new LeetcodeServiceException("Questions from Leetcode were null.");

            // Map to ProblemGroup
            return new ProblemGroup
            {
                TotalNum = questions.Count,
                Problems = questions.Select(question => new ProblemEntry
                    {
                        Information = new Information
                        {
                          Id = question.questionId,
                          Title = question.title,
                          Slug = question.titleSlug
                        },
                        Difficulty = new Difficulty
                        {
                            Level = Enum.Parse<DifficultyLevel>(((string) question.difficulty).Capitalize())
                        },
                        IsPaidOnly = question.isPaidOnly
                    }
                ).ToList()
            };
        }

        public async Task<Problem> GetProblem(string titleSlug)
        {
            var queryResponse = await GraphQLRequest<dynamic>(new GetProblemDetailGraphQLRequest(titleSlug));

            var question = queryResponse.question;

            List<dynamic> codeSnippets = AsDynamicList(question.codeSnippets);
            List<dynamic> topicTags = AsDynamicList(question.topicTags);

            return new Problem
            {
                Id = question.questionId,
                Slug = titleSlug,
                Link = ProblemLink(titleSlug),
                Title = question.title,
                Content = question.content,
                Likes = question.likes,
                Dislikes = question.dislikes,
                PaidOnly = question.isPaidOnly,
                TestCase = question.sampleTestCase,
                Difficulty = Enum.Parse<DifficultyLevel>((string) question.difficulty, ignoreCase: true),
                CodeSnippets = codeSnippets?.Select(snippet =>
                    new CodeSnippet
                    {
                        Language = snippet.lang,
                        Code = snippet.code
                    }
                ).ToList(),
                Tags = topicTags?.Select(tag => (string) tag.name).ToList()
            };
        }

        private async Task<T> GraphQLRequest<T>(GraphQLRequest request)
        {
            var response = await _graphQlHttpClient.SendQueryAsync<T>(request);

            if (response.Errors?.Length > 0)
            {
                var errors = response.Errors
                    .Select(error => $"Error Path {error.Path} Message {error.Message}")
                    .JoinToString();
                
                throw new LeetcodeServiceException($"GraphQL Request Failed with Errors [{errors}]");
            }

            return response.Data;
        }

        private async Task<T> HttpRequest<T>(Func<Task<HttpResponseMessage>> requestFunction)
        {
            var response = await requestFunction();
            if (!response.IsSuccessStatusCode)
            {
                throw new LeetcodeServiceException($"Http Request Failed with Status Code [{response.StatusCode}]");
            }

            return _jsonMapper.Deserialize<T>(await response.Content.ReadAsStringAsync());
        }

        private List<dynamic> AsDynamicList(dynamic obj) => ((JArray) obj)?.ToObject<List<dynamic>>();

        private string ProblemLink(string slug) => $"https://leetcode.com/problems/{slug}/";
    }
    internal static class LeetcodeUris
    {
        public static string Leetcode = "https://leetcode.com/api/";
        public static string AllProblems = "https://leetcode.com/api/problems/all/";
        public static string GraphQL = "https://leetcode.com/graphql";
    }
}