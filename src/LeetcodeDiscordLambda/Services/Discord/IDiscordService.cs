using System.Threading.Tasks;
using LeetcodeDiscordLambda.Services.Discord.Types;

namespace LeetcodeDiscordLambda.Services.Discord
{
    public interface IDiscordService
    {
        Task SendMessageToChannel(SendMessageToChannelRequest request);
    }
}