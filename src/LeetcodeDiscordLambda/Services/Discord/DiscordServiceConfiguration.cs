using System.Net.Http;
using LeetcodeDiscordLambda.Core.Json;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Services.Discord
{
    public static class DiscordServiceConfiguration
    {
        private static string HttpClient = "DiscordHttpClient";
        
        public static void ConfigureDiscordService(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddHttpClient(HttpClient);

            serviceCollection.AddSingleton<IDiscordService, DiscordService>(s => new DiscordService(
                s.GetService<IHttpClientFactory>().CreateClient(HttpClient),
                s.GetRequiredService<IJsonMapper>()
            ));
        }
    }
}