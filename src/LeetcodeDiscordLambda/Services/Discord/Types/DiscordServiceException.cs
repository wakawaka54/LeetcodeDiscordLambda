using System;
using LeetcodeDiscordLambda.Services.Types;

namespace LeetcodeDiscordLambda.Services.Discord.Types
{
    public class DiscordServiceException : ServiceException
    {
        public DiscordServiceException(string message) : base(message)
        {
        }

        public DiscordServiceException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}