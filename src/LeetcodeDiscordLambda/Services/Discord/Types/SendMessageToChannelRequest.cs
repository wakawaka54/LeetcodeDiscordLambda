using System.Text.Json.Serialization;

namespace LeetcodeDiscordLambda.Services.Discord.Types
{
    /// <summary>
    /// Request to send a message to a channel.
    /// </summary>
    public class SendMessageToChannelRequest
    {
        [JsonPropertyName("content")]
        public string Content { get; set; }
        
        [JsonPropertyName("username")]
        public string Username { get; set; }
        
        [JsonPropertyName("avatar_url")]
        public string AvatarUrl { get; set; }
        
        [JsonPropertyName("allowed_mentions")]
        public bool AllowedMentions { get; set; }
        
        [JsonIgnore]
        public string WebhookUrl { get; set; }
    }
}