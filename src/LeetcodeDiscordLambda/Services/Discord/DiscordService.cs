using System;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using LeetcodeDiscordLambda.Core.Json;
using LeetcodeDiscordLambda.Services.Discord.Types;

namespace LeetcodeDiscordLambda.Services.Discord
{
    /// <summary>
    /// Provides methods for accessing Discord API.
    /// </summary>
    public class DiscordService : IDiscordService
    {
        private readonly HttpClient _httpClient;
        private readonly IJsonMapper _jsonMapper;

        public DiscordService(HttpClient httpClient, IJsonMapper jsonMapper)
        {
            _httpClient = httpClient;
            _jsonMapper = jsonMapper;
        }
        
        public Task SendMessageToChannel(SendMessageToChannelRequest request)
        {
            return HttpRequest<HttpResponseMessage>(() =>
                _httpClient.PostAsync(
                    request.WebhookUrl,
                    CreateStringContent(request)
                )
            );
        }

        private async Task<T> HttpRequest<T>(Func<Task<HttpResponseMessage>> requestFn)
        {
            var response = await requestFn();

            if (!response.IsSuccessStatusCode)
            {
                throw new DiscordServiceException($"Http Request Failed with Status Code [{response.StatusCode}]");
            }

            // Check if the response needs to be converted.
            if (response is T responseType)
            {
                return responseType;
            }
            
            return _jsonMapper.Deserialize<T>(await response.Content.ReadAsStringAsync());
        }

        private StringContent CreateStringContent(object any) =>
            new StringContent(_jsonMapper.Serialize(any), Encoding.UTF8, MediaTypeNames.Application.Json);
    }
}