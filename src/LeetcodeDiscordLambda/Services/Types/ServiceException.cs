using System;

namespace LeetcodeDiscordLambda.Services.Types
{
    /// <summary>
    /// Service based exception.
    /// </summary>
    public class ServiceException : Exception
    {
        public ServiceException(string message) : base(message)
        {
            
        }

        public ServiceException(string message, Exception innerException) : base(message, innerException)
        {
            
        }
    }
}