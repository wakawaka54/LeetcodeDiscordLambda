using LeetcodeDiscordLambda.Services.Discord;
using LeetcodeDiscordLambda.Services.Leetcode;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Services
{
    public static class ServicesConfiguration
    {
        public static void ConfigureServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.ConfigureLeetcodeService();
            serviceCollection.ConfigureDiscordService();
        }
    }
}