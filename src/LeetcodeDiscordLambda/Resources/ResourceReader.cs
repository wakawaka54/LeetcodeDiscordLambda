using System;
using System.IO;
using System.Reflection;

namespace LeetcodeDiscordLambda.Resources
{
    /// <summary>
    /// Utility type for reading resources.
    /// </summary>
    public class ResourceReader
    {
        private const string ResourcePath = "LeetcodeDiscordLambda.Resources";

        public Stream ReadStream(string fileName)
        {
            string resourcePath = String.Join(".", ResourcePath, fileName);
            return GetType().Assembly.GetManifestResourceStream(resourcePath) ??
                   throw new ArgumentException($"Could not find resource {fileName}");
        }

        public string ReadString(string fileName)
        {
            return ReadStream(fileName).ReadToString();
        }
    }
}