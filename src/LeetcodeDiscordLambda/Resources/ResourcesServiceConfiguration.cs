using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Resources
{
    /// <summary>
    /// Dependency injection configuration to configure the resources into the DI injection context.
    /// </summary>
    public static class ResourcesServiceConfiguration
    {
        public static void ConfigureResources(this IServiceCollection collection)
        {
            collection.AddSingleton<ResourceReader>();
        }
    }
}