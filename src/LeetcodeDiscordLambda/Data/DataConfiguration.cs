using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Extensions.NETCore.Setup;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Data
{
    public static class DataConfiguration
    {
        public static void ConfigureData(this IServiceCollection service)
        {
            service.AddAWSService<IAmazonDynamoDB>();
            service.AddSingleton<CodeQuestionRepository>();

            service.AddSingleton<IDynamoDBContext, DynamoDBContext>(s =>
                new DynamoDBContext(
                    s.GetRequiredService<IAmazonDynamoDB>(),
                    new DynamoDBContextConfig
                    {
                        TableNamePrefix = "beta-"
                    }
                )
            );
        }
    }
}