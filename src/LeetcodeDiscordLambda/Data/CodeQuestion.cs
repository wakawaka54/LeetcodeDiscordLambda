using System;
using Amazon.DynamoDBv2.DataModel;

namespace LeetcodeDiscordLambda.Data
{
    [DynamoDBTable("code-questions")]
    public class CodeQuestion
    {
        [DynamoDBHashKey("id")]
        public string Id { get; set; }
        
        public DateTime Created { get; set; } = DateTime.Now;
    }
}