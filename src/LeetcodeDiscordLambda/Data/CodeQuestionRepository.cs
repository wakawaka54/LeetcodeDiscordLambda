using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

namespace LeetcodeDiscordLambda.Data
{
    /// <summary>
    /// Repository Accessor type for fetching and saving CodeQuestions. 
    /// </summary>
    public class CodeQuestionRepository
    {
        private readonly IDynamoDBContext _dynamoDbContext;

        public CodeQuestionRepository(IDynamoDBContext dynamoDbContext)
        {
            _dynamoDbContext = dynamoDbContext;
        }

        public Task SaveAsync(CodeQuestion question)
        {
            return _dynamoDbContext.SaveAsync(question);
        }

        public Task<CodeQuestion> GetAsync(string id)
        {
            return _dynamoDbContext.LoadAsync(new CodeQuestion
            {
                Id = id
            });
        }

        public Task DeleteAsync(string id)
        {
            return _dynamoDbContext.DeleteAsync(new CodeQuestion
            {
                Id = id
            });
        }
    }
}