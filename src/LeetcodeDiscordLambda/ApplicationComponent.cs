using System;
using LeetcodeDiscordLambda.Core;
using LeetcodeDiscordLambda.Core.Json;
using LeetcodeDiscordLambda.Data;
using LeetcodeDiscordLambda.Domain.Questions;
using LeetcodeDiscordLambda.Resources;
using LeetcodeDiscordLambda.Services;
using LeetcodeDiscordLambda.Services.Discord;
using LeetcodeDiscordLambda.Services.Leetcode;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda
{
    /// <summary>
    /// Configures the application and provides types to Lambda handlers.
    /// </summary>
    public interface IApplicationComponent
    {
        public IQuestionEvaluator QuestionEvaluator { get; }
        
        public IQuestionPublisher QuestionPublisher { get; }
        
        public IJsonMapper JsonMapper { get; }
    }

    public class ApplicationComponent : IApplicationComponent
    {
        public IQuestionEvaluator QuestionEvaluator { get; }
        
        public IQuestionPublisher QuestionPublisher { get; }
        
        public IJsonMapper JsonMapper { get; }

        public ApplicationComponent()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            
            serviceCollection.ConfigureCore();
            serviceCollection.ConfigureData();
            serviceCollection.ConfigureDomainQuestions();
            serviceCollection.ConfigureResources();
            serviceCollection.ConfigureServices();

            // Create the dependency injection provider.
            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            
            // Inject in all the required types.
            QuestionEvaluator = serviceProvider.GetRequiredService<IQuestionEvaluator>();
            QuestionPublisher = serviceProvider.GetRequiredService<IQuestionPublisher>();
            JsonMapper = serviceProvider.GetRequiredService<IJsonMapper>();
        }
    }
}