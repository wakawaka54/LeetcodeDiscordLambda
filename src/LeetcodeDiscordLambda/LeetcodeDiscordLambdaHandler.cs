
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using LeetcodeDiscordLambda.Core.Json;
using LeetcodeDiscordLambda.Domain.Questions;
using LeetcodeDiscordLambda.Domain.Questions.Destination;
using LeetcodeDiscordLambda.Services.Leetcode.Types;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace LeetcodeDiscordLambda
{
    /// <summary>
    /// Primary Post Discord Question Lambda Handler.
    /// </summary>
    public class LeetcodeDiscordLambdaHandler
    {
        private readonly IQuestionEvaluator _questionEvaluator;
        private readonly IQuestionPublisher _questionPublisher;
        private readonly IJsonMapper _jsonMapper;

        /// <summary>
        /// Default constructor called by the Lambda runtime.
        /// </summary>
        public LeetcodeDiscordLambdaHandler() : this(new ApplicationComponent()) { }
        
        public LeetcodeDiscordLambdaHandler(IApplicationComponent applicationComponent)
        {
            _questionEvaluator = applicationComponent.QuestionEvaluator;
            _questionPublisher = applicationComponent.QuestionPublisher;
            _jsonMapper = applicationComponent.JsonMapper;
        }
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="inputStream">Input stream which contains data from Lambda request.</param>
        /// <param name="context">Lambda AWS context.</param>
        /// <returns></returns>
        public async Task<Stream> FunctionHandler(Stream inputStream, ILambdaContext context)
        {
            // Deserialize the input
            var request = _jsonMapper.Deserialize<DiscordLeetcodePostQuestionRequest>(inputStream);
            
            var destinationChannel = new DiscordChannel
            {
                Id = request.DiscordChannelId,
                WebhookUrl = request.DiscordWebhookUrl
            };
            
            var question = await _questionEvaluator.Evaluate(new QuestionEvaluationContext
            {
                DestinationChannel = destinationChannel,
                SelectionCriteria = new SelectionCriteria
                {
                    Tags = request.LeetcodeTags,
                    Levels = request.LeetcodeDifficulties
                }
            });

            await _questionPublisher.Publish(destinationChannel, question);

            // Return the asked question.
            return _jsonMapper.Serialize(question).ToStream();
        }
    }

    public class DiscordLeetcodePostQuestionRequest
    {
        public string DiscordChannelId { get; set; }
        public string DiscordWebhookUrl { get; set; }
        public List<string> LeetcodeTags { get; set; } = new List<string>();
        
        public List<DifficultyLevel> LeetcodeDifficulties { get; set; } = new List<DifficultyLevel>();
    }
}
