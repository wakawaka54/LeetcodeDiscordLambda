using LeetcodeDiscordLambda.Domain.Questions;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Domain
{
    public static class DomainServiceConfiguration
    {
        public static void ConfigureDomain(this IServiceCollection collection)
        {
            collection.ConfigureDomainQuestions();
        }
    }
}