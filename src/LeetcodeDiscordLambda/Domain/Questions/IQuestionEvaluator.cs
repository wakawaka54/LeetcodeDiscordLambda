using System.Threading.Tasks;

namespace LeetcodeDiscordLambda.Domain.Questions
{
    public interface IQuestionEvaluator
    {
        public Task<Question> Evaluate(QuestionEvaluationContext context);
    }
}