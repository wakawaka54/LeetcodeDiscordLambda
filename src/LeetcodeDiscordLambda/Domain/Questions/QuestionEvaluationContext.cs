using System.Collections.Generic;
using LeetcodeDiscordLambda.Domain.Questions.Destination;
using LeetcodeDiscordLambda.Services.Leetcode.Types;

namespace LeetcodeDiscordLambda.Domain.Questions
{
    /// <summary>
    /// Context used when evaluating what question to select.
    /// </summary>
    public class QuestionEvaluationContext
    {
        public IDestinationChannel DestinationChannel { get; set; }
        public SelectionCriteria SelectionCriteria { get; set; }
    }

    public class SelectionCriteria
    {
        public List<string> Tags { get; set; }
        public List<DifficultyLevel> Levels { get; set; }
    }
}