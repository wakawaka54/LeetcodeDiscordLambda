using System.Linq;
using System.Threading.Tasks;
using LeetcodeDiscordLambda.Domain.Questions.Destination;

namespace LeetcodeDiscordLambda.Domain.Questions
{
    /// <summary>
    /// Publishes a question to a destination.
    /// </summary>
    public class QuestionPublisher : IQuestionPublisher
    {
        private readonly DestinationPublisherProvider _destinationPublisherProvider;

        public QuestionPublisher(DestinationPublisherProvider destinationPublisherProvider)
        {
            _destinationPublisherProvider = destinationPublisherProvider;
        }

        public async Task Publish<T>(T channel, Question question) where T : IDestinationChannel
        {
            await Task.WhenAll(
                _destinationPublisherProvider.Get<T>()
                    .Select(publisher => publisher.Publish(channel, question))
                    .ToList()
            );
        }
    }
}