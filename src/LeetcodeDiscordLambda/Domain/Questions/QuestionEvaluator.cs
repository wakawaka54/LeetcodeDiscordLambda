using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading.Tasks;
using LeetcodeDiscordLambda.Data;
using LeetcodeDiscordLambda.Domain.Questions.Destination;
using LeetcodeDiscordLambda.Domain.Questions.Selector;
using LeetcodeDiscordLambda.Domain.Questions.Validator;

namespace LeetcodeDiscordLambda.Domain.Questions
{
    /// <summary>
    /// Evaluator which evaluates what the next question should be.
    /// </summary>
    public class QuestionEvaluator : IQuestionEvaluator
    {
        private readonly IQuestionSelector _questionSelector;
        private readonly CodeQuestionRepository _codeQuestionRepository;
        private readonly List<IQuestionValidator> _questionValidators;

        public QuestionEvaluator(
            IQuestionSelector selector,
            CodeQuestionRepository repository,
            IEnumerable<IQuestionValidator> validators
        ) {
            _questionSelector = selector;
            _codeQuestionRepository = repository;
            _questionValidators = validators.ToList();
        }

        public async Task<Question> Evaluate(QuestionEvaluationContext context)
        {
            Question selectedQuestion;

            do
            {
                // TODO: Create a condition where we give up if we have exhausted the questions
                selectedQuestion = await _questionSelector.SelectAsync(context);
            } while (selectedQuestion != null && !await IsQuestionValid(selectedQuestion, context));

            if (selectedQuestion != null)
            {
                // Persist the question into the repository
                await _codeQuestionRepository.SaveAsync(CodeQuestionUtils.CreateCodeQuestion(context, selectedQuestion));
            }

            return selectedQuestion;
        }

        private async Task<bool> IsQuestionValid(Question question, QuestionEvaluationContext context) =>
            await _questionValidators
                .ToObservable()
                .SelectMany(validator => validator.Validate(context, question).ToObservable())
                .All(result => result.Valid);
    }
}