using LeetcodeDiscordLambda.Domain.Questions.Destination;
using LeetcodeDiscordLambda.Domain.Questions.Selector;
using LeetcodeDiscordLambda.Domain.Questions.Validator;
using LeetcodeDiscordLambda.Resources;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Domain.Questions
{
    public static class QuestionsServiceConfiguration
    {
        public static void ConfigureDomainQuestions(this IServiceCollection collection)
        {
            collection.AddSingleton<DiscordDestinationOptions>(s =>
                new DiscordDestinationOptions
                {
                    Template = s.GetRequiredService<ResourceReader>().ReadString("DiscordProblemTemplate.liquid")
                }
            );
            
            collection.AddSingleton<IQuestionSelector, LeetcodeQuestionSelector>();
            collection.AddSingleton<IQuestionValidator, HistoricalQuestionValidator>();
            collection.AddSingleton<IDestinationPublisher<DiscordChannel>, DiscordDestinationPublisher>();
            collection.AddSingleton<DestinationPublisherProvider>();

            collection.AddSingleton<IQuestionEvaluator, QuestionEvaluator>();
            collection.AddSingleton<IQuestionPublisher, QuestionPublisher>();
        }
    }
}