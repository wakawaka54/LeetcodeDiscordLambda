using System.Collections.Generic;
using LeetcodeDiscordLambda.Services.Leetcode.Types;

namespace LeetcodeDiscordLambda.Domain.Questions
{
    public class Question
    {
        public string Id { get; set; }
        
        public string Title { get; set; }
        
        public List<string> Tags { get; set; } = new List<string>();

        public DifficultyLevel Difficulty { get; set; }
        public string Content { get; set; }
        
        public string TestCase { get; set; }
        public string Link { get; set; }
        
        public List<CodeSnippet> Snippets { get; set; } = new List<CodeSnippet>();
    }

    public class CodeSnippet
    {
        public string Content { get; set; }
        public string Language { get; set; }
    }
}