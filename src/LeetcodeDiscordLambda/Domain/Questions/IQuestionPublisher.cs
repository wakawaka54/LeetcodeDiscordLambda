using System.Threading.Tasks;
using LeetcodeDiscordLambda.Domain.Questions.Destination;

namespace LeetcodeDiscordLambda.Domain.Questions
{
    public interface IQuestionPublisher
    {
        public Task Publish<T>(T channel, Question question) where T : IDestinationChannel;
    }
}