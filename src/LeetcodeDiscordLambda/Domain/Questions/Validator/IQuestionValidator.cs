using System.Threading.Tasks;

namespace LeetcodeDiscordLambda.Domain.Questions.Validator
{
    /// <summary>
    /// Validator interface which validates whether we should ask a question.
    /// </summary>
    public interface IQuestionValidator
    {
        Task<QuestionValidationResult> Validate(QuestionEvaluationContext context, Question question);
    }

    public class QuestionValidationResult
    {
        public static QuestionValidationResult ValidResult() => new QuestionValidationResult(true);
        
        public static QuestionValidationResult InvalidResult() => new QuestionValidationResult(false);

        
        public bool Valid { get; }

        public QuestionValidationResult(bool valid)
        {
            Valid = valid;
        }
    }
}