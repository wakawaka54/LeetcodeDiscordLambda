using System.Threading.Tasks;
using LeetcodeDiscordLambda.Data;

namespace LeetcodeDiscordLambda.Domain.Questions.Validator
{
    /// <summary>
    /// Validator which checks if the question has been asked historically.
    /// </summary>
    public class HistoricalQuestionValidator : IQuestionValidator
    {
        private readonly CodeQuestionRepository _codeQuestionRepository;

        public HistoricalQuestionValidator(CodeQuestionRepository codeQuestionRepository)
        {
            _codeQuestionRepository = codeQuestionRepository;
        }
        
        public async Task<QuestionValidationResult> Validate(QuestionEvaluationContext context, Question question)
        {
            var result = await _codeQuestionRepository.GetAsync(
                CodeQuestionUtils.CreateCodeQuestion(context, question).Id
            );
            
            return result != null ?
                QuestionValidationResult.InvalidResult() :
                QuestionValidationResult.ValidResult();
        }
    }
}