namespace LeetcodeDiscordLambda.Domain.Questions.Destination
{
    public class DiscordChannel : IDestinationChannel
    {
        public string Id { get; set; }
        
        public string WebhookUrl { get; set; }
    }
}