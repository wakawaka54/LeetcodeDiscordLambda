using System.Threading.Tasks;

namespace LeetcodeDiscordLambda.Domain.Questions.Destination
{
    /// <summary>
    /// Publisher which publishes a question to a destination.
    /// </summary>
    public interface IDestinationPublisher<TDestination> where TDestination : IDestinationChannel
    {
        Task Publish(TDestination destination, Question question);
    }
}