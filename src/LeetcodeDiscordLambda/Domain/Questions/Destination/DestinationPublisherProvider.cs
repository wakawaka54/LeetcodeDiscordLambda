using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Domain.Questions.Destination
{
    /// <summary>
    /// Provides destination publishers based on a destination type.
    /// </summary>
    public class DestinationPublisherProvider
    {
        private readonly IServiceProvider _serviceProvider;

        public DestinationPublisherProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public List<IDestinationPublisher<T>> Get<T>() where T : IDestinationChannel =>
            _serviceProvider.GetServices<IDestinationPublisher<T>>().ToList();
    }
}