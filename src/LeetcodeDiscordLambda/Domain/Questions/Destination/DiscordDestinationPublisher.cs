using System.Threading.Tasks;
using Html2Markdown;
using LeetcodeDiscordLambda.Services.Discord;
using LeetcodeDiscordLambda.Services.Discord.Types;
using Scriban;

namespace LeetcodeDiscordLambda.Domain.Questions.Destination
{
    public class DiscordDestinationPublisher : IDestinationPublisher<DiscordChannel>
    {
        private readonly IDiscordService _discordService;
        private readonly DiscordDestinationOptions _options;
        private readonly Converter _htmlConverter = new Converter();

        public DiscordDestinationPublisher(IDiscordService discordService, DiscordDestinationOptions options)
        {
            _discordService = discordService;
            _options = options;
        }
        
        public async Task Publish(DiscordChannel destination, Question question)
        {
            await _discordService.SendMessageToChannel(new SendMessageToChannelRequest
            {
                Content = ContentTemplate(question),
                WebhookUrl = destination.WebhookUrl
            });
        }

        private string ContentTemplate(Question question)
        {
            return Template.Parse(_options.Template)
                .Render(new
                {
                    title = question.Title,
                    link = question.Link,
                    tags = question.Tags,
                    difficulty = question.Difficulty,
                    content = ContentFixUp(question.Content)
                }
            );
        }

        private string ContentFixUp(string content)
        {
            return _htmlConverter.Convert(content ?? "No Description")
                .RemoveRepeating('\n');
        }
    }

    public class DiscordDestinationOptions
    {
        public string Template { get; set; }
    }
}