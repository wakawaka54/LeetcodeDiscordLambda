using System;
using System.Collections.Generic;
using LeetcodeDiscordLambda.Data;
using LeetcodeDiscordLambda.Domain.Questions.Destination;

namespace LeetcodeDiscordLambda.Domain.Questions
{
    /// <summary>
    /// Utility methods for Code Questions.
    /// </summary>
    public static class CodeQuestionUtils
    {
        public static CodeQuestion CreateCodeQuestion(QuestionEvaluationContext context, Question question) =>
            new CodeQuestion
            {
                Id = CreateCodeQuestionId(context, question),
                Created = DateTime.Now
            };

        private static string CreateCodeQuestionId(QuestionEvaluationContext context, Question question)
        {
            var components = new List<string>();
            var channel = context.DestinationChannel;

            if (channel is DiscordChannel discordChannel)
            {
                components.Add(discordChannel.Id);
            }
            else
            {
                throw new ArgumentException($"Unknown channel input for computing ID {context.DestinationChannel.GetType()}");
            }
            
            components.Add(question.Id);

            return String.Join(".", components);
        }
    }
}