using System.Threading.Tasks;

namespace LeetcodeDiscordLambda.Domain.Questions.Selector
{
    public interface IQuestionSelector
    {
        Task<Question> SelectAsync(QuestionEvaluationContext  context);
    }
}