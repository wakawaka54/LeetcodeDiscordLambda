using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using LeetcodeDiscordLambda.Services.Leetcode;
using LeetcodeDiscordLambda.Services.Leetcode.Types;

namespace LeetcodeDiscordLambda.Domain.Questions.Selector
{
    public class LeetcodeQuestionSelector : IQuestionSelector
    {
        private readonly ILeetcodeService _leetcodeService;
        private readonly IDictionary<string, ProblemGroup> _cachedProblemGroups =
            new ConcurrentDictionary<string, ProblemGroup>();

        public LeetcodeQuestionSelector(ILeetcodeService leetcodeService)
        {
            _leetcodeService = leetcodeService;
        }

        public async Task<Question> SelectAsync(QuestionEvaluationContext context)
        {
            var problems = await context.SelectionCriteria.Tags
                .ToObservable()
                .Select(GetProblems)
                .SelectMany(s => s);

            var randomProblem = problems
                .Where(problem => MatchProblemEntry(problem, context))
                .SelectRandom();
            
            return ConvertQuestion(await _leetcodeService.GetProblem(randomProblem.Information.Slug));
        }

        private async Task<List<ProblemEntry>> GetProblems(string tag)
        {
            if (!_cachedProblemGroups.ContainsKey(tag))
            {
                var result = await _leetcodeService.GetProblemsByTagAsync(tag);
                _cachedProblemGroups[tag] = result;
            }

            return _cachedProblemGroups[tag].Problems;
        }

        /// <summary>
        /// Matches a problem entry based on a list of predicates.
        /// </summary>
        /// <param name="entry">The problem entry.</param>
        /// <param name="context">The evaluation context.</param>
        /// <returns>Whether the problem should be matched.</returns>
        private bool MatchProblemEntry(ProblemEntry entry, QuestionEvaluationContext context) =>
            new List<Predicate<ProblemEntry>>
            {
                _ => context.SelectionCriteria.Levels.Contains(entry.Difficulty.Level),
                _ => !entry.IsPaidOnly
            }
            .All(predicate => predicate(entry));

        /// <summary>
        /// Convert a Leetcode Problem to Question.
        /// </summary>
        /// <param name="problem">Leetcode Problem</param>
        /// <returns></returns>
        private Question ConvertQuestion(Problem problem) =>
            new Question
            {
                Id = problem.Id,
                Title = problem.Title,
                Content = problem.Content,
                Difficulty = problem.Difficulty,
                Tags = problem.Tags,
                Snippets = problem.CodeSnippets?.Select(snippet =>
                    new CodeSnippet
                    {
                        Content = snippet.Code,
                        Language = snippet.Language
                    }
                ).ToList(),
                Link = problem.Link,
                TestCase = problem.TestCase
            };
    }
}