using Amazon.Extensions.NETCore.Setup;
using LeetcodeDiscordLambda.Core.DependencyInjection;
using LeetcodeDiscordLambda.Core.Json;
using Microsoft.Extensions.DependencyInjection;

namespace LeetcodeDiscordLambda.Core
{
    /// <summary>
    /// Configures the dependency injection context with services needed by the Lambda handler.
    /// </summary>
    public static class CoreServiceConfiguration
    {
        public static void ConfigureCore(this IServiceCollection collection)
        {
            // Bind core service types.
            collection.AddSingleton<IJsonMapper, JsonMapper>();

            collection.AddOptions();
            
            collection.AddNamedServices();
            
            // Configure AWS with the default credentials provider
            // It'll look at ENV variables first and then fallback to local credentials
            collection.AddDefaultAWSOptions(new AWSOptions());
        }
    }
}