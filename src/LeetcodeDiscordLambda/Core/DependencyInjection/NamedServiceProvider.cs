using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace LeetcodeDiscordLambda.Core.DependencyInjection
{
    /// <summary>
    /// Creates a DI provider which provides implementations based on name.
    /// </summary>
    public class NamedServiceProvider
    {
        private readonly IServiceProvider _serviceProvider;

        public NamedServiceProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        
        public T GetNamed<T>(string name)
        {
            var namedOptionsSnapshotAccessor = _serviceProvider.GetService<IOptionsSnapshot<NamedServiceOptions<T>>>();

            var typeName = typeof(T);
            var snapshot = namedOptionsSnapshotAccessor.Get(typeName.FullName + name);

            return snapshot.ServiceProvider(_serviceProvider);
        }
    }

    public static class NamedServicesExtensions
    {
        public static T GetNamed<T>(this IServiceProvider serviceProvider, string name)
        {
            return serviceProvider.GetService<NamedServiceProvider>().GetNamed<T>(name);
        }

        public static void AddNamedServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<NamedServiceProvider>();
        }
        
        public static void AddNamedService<T>(this IServiceCollection serviceCollection, string name, Func<IServiceProvider, T> provider)
        {
            serviceCollection.AddOptions<NamedServiceOptions<T>>();

            var configurationName = typeof(T).FullName + name;
            serviceCollection.Configure<NamedServiceOptions<T>>(configurationName, options => options.ServiceProvider = provider);
        }
    }

    public class NamedServiceOptions<T>
    {
        public Func<IServiceProvider, T> ServiceProvider { get; set; }
    }
}