using System;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using Amazon.Lambda.Serialization.SystemTextJson;
using Amazon.Lambda.Serialization.SystemTextJson.Converters;

namespace LeetcodeDiscordLambda.Core.Json
{
    public class JsonMapper : IJsonMapper
    {
        public static JsonSerializerOptions StandardOptions = new JsonSerializerOptions
        {
            IgnoreNullValues = true,
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = new AwsNamingPolicy(),
            Converters =
            {
                new DateTimeConverter(),
                new ConstantClassConverter(),
                new MemoryStreamConverter(),
                new JsonStringEnumConverter()
            }
        };
        
        private JsonSerializerOptions SerializerOptions { get; }

        public JsonMapper()
        {
            // Create the standard serializer options.
            SerializerOptions = StandardOptions;
        }

        public void Serialize(Object obj, Stream outputStream)
        {
            using Utf8JsonWriter writer = new Utf8JsonWriter(outputStream);
            JsonSerializer.Serialize(writer, obj, SerializerOptions);
        }

        public string Serialize(object obj) => JsonSerializer.Serialize(obj, SerializerOptions);

        public T Deserialize<T>(Stream inputStream)
        {
            MemoryStream memoryStream = new MemoryStream();
            inputStream.CopyTo(memoryStream);
            var bytes = memoryStream.ToArray();
            return JsonSerializer.Deserialize<T>(bytes, SerializerOptions);
        }

        public T Deserialize<T>(string jsonString) => JsonSerializer.Deserialize<T>(jsonString, SerializerOptions);
    }
}