using System;
using System.IO;

namespace LeetcodeDiscordLambda.Core.Json
{
    /// <summary>
    /// Interface which defines a general Json Mapper.
    /// </summary>
    public interface IJsonMapper
    {
        /// <summary>
        /// Serializes an object into a JSON output stream.
        /// </summary>
        /// <param name="obj">Object to serialize.</param>
        /// <param name="outputStream">Output stream.</param>
        void Serialize(Object obj, Stream outputStream);
        
        /// <summary>
        /// Serializes an object into a JSON string.
        /// </summary>
        /// <param name="obj">Object to serialize.</param>
        /// <returns>JSON formatted string.</returns>
        String Serialize(Object obj);

        /// <summary>
        /// Deserializes a stream of JSON formatted data into a typed object.
        /// </summary>
        /// <param name="inputStream">JSON formatted stream data.</param>
        /// <typeparam name="T">Object type.</typeparam>
        /// <returns>Deserialized object type.</returns>
        T Deserialize<T>(Stream inputStream);
        
        /// <summary>
        /// Deserializes a string JSON payload into a typed object.
        /// </summary>
        /// <param name="jsonString">Valid JSON string.</param>
        /// <typeparam name="T">Object type.</typeparam>
        /// <returns>Deserialized object type.</returns>
        T Deserialize<T>(String jsonString);
    }
}